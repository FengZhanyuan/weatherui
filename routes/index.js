﻿'use strict';
var express = require('express');
var router = express.Router();
var Client = require('node-rest-client').Client;
var utils = require("../utils/common");
var mcache = require("memory-cache")
var client = new Client();


function analysis(data) {
    let date = utils.getWeatherDate(data.result.date);
    let dailys = data.result.daily;
    let city = "Shang hai"//data.result.city;
    let weatherDays = [];
    for (let index in dailys) {
        let date = utils.getWeatherDate(dailys[index].date);
        let temperature = dailys[index].day.temphigh;
        let weather = utils.getWeatherStyle(dailys[index].day.weather);
        let weatherDay = {
            city: city,
            date: date,
            type: weather.type,
            temperature: temperature,
            style: weather.style,
            name: weather.name
        }
        if (index < 5) {
            weatherDays.push(weatherDay)
        }
    }
    return weatherDays
}

/* GET home page. */
router.post('/', function (req, res) {
    let weather_data = mcache.get("weather")
    if (weather_data) {
        let weatherDays = analysis(weather_data)
        res.send({ weathers: weatherDays });
        return
    } else {
        let args = {
            path: { "cityid": "24" },
            headers: { "Content-Type": "application/json", "Authorization": "APPCODE c74f12fb5b2b4bb595f80fe170417b6d" }
        }

        //获取数据天气的数据
        client.get("http://jisutqybmf.market.alicloudapi.com/weather/query?cityid=${cityid}", args, (data, response) => {
            mcache.put("weather", data, 3600 * 1000, function (key, value) {
                console.log(key + " cached " + value)
            })
            let weatherDays = analysis(data)
            //组合数据
            res.send({ weathers: weatherDays });
        })
    }

    //获取时间

});

module.exports = router;
