﻿'use strict';
const weekdays = ["Sunday ", "Monday", "Tuesday", "Wednesday ", "Thursday", "Friday", "Saturday"];
const months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
const weatherMaps = new Map([
    ["晴", { style: "wi-day-sunny", type: "sun", name: "sun" }],
    ["多云", { style: "wi-day-cloudy", type: "wind", name: "partly cloudy" }],
    ["阴", { style: "wi-cloudy", type: "rain", name: "cloudy"  }],
    ["阵雨", { style: "wi-day-showers", type: "rain", name: "shower"  }],
    ["雷阵雨", { style: "wi-day-storm-showers", type: "rain", name: "Thunder shower" }],
    ["雷阵雨伴有冰雹", { style: "wi-day-thunderstorm", type: "rain", name: "shower with hail"  }],
    ["雨夹雪", { style: "wi-snow-wind", type: "rain", name: "Sleet"  }],
    ["小雨", { style: "wi-rain-mix", type: "rain", name: "Light rain"  }],
    ["中雨", { style: "wi-hail", type: "rain", name: "Rain"  }],
    ["大雨", { style: "wi-rain", type: "thunder", name: "heavy rain"  }],
    ["暴雨", { style: "wi-rain", type: "thunder", name: "rainstorm" }],
    ["大暴雨", { style: "wi-rain", type: "thunder", name: "rainstorm" }],
    ["特大暴雨", { style: "wi-rain", type: "thunder", name: "rainstorm" }],
    ["阵雪", { style: "wi-snow", type: "snow", name: "Snow shower"  }],
    ["小雪", { style: "wi-snow", type: "snow", name: "Light snow" }],
    ["中雪", { style: "wi-snow", type: "snow", name: "snow"  }],
    ["大雪", { style: "wi-snowflake-cold", type: "snow", name: "Heavy snow" }],
    ["暴雪", { style: "wi-snowflake-cold", type: "snow", name: "Heavy snow" }],
    ["雾", { style: "wi-fog", type: "wind", name: "fog"  }],
    ["冻雨", { style: "wi-sprinkle", type: "rain", name: "Frozen rain"  }],
    ["沙尘暴", { style: "wi-sandstorm", type: "wind", name: "Sandstorm" }],
    ["小雨-中雨", { style: "wi-hail", type: "rain", name: "Light rain - moderate rain"  }],
    ["中雨-大雨", { style: "wi-rain", type: "rain", name: "Moderate rain - heavy rain"  }],
    ["大雨-暴雨", { style: "wi-rain", type: "thunder", name: "heavy rain"  }],
    ["暴雨-大暴雨", { style: "wi-rain", type: "thunder", name: "rainstorm"  }],
    ["大暴雨-特大暴雨", { style: "wi-rain", type: "thunder", name: "rainstorm"  }],
    ["小雪-中雪", { style: "wi-snow", type: "snow", name: "Light Snow - snow"  }],
    ["中雪-大雪", { style: "wi-snow", type: "snow", name: "snow"  }],
    ["大雪-暴雪", { style: "wi-snow", type: "snow", name: "Heavy snow"  }],
    ["浮尘", { style: "wi-dust", type: "wind", name: "Floating dust"  }],
    ["扬沙", { style: "wi-dust", type: "wind", name: "Yangsha"  }],
    ["强沙尘暴", { style: "wi-sandstorm", type: "wind", name: "Strong sandstorm" }],
    ["强浓雾", { style: "wi-sandstorm", type: "wind", name: "Strong fog"  }],
    ["霾", { style: "wi-windy", type: "wind", name: "haze"  }],
    ["中毒霾", { style: "wi-windy", type: "wind", name: "Poisoning"  }],
    ["重度霾", { style: "wi-sandstorm", type: "wind", name: "Strong haze" }],
    ["大雾", { style: "wi-fog", type: "wind", name: "Heavy fog"  }],
    ["特强浓雾", { style: "wi-fog", type: "wind", name: "Extra strong fog"  }],
    ["无", { style: "na", type: "wind", name: "N/A"  }],
    ["雨", { style: "wi-rain", type: "rain", name: "Rain"  }],
    ["雪", { style: "wi-snow", type: "snow", name: "Snow" }]
]);


function getFormateDate(date) {
    let week = date.getDay();
    let month = date.getMonth();
    let day = date.getDate();
    return weekdays[week] + " " + day + " " + months[month];
}



var utils = {
    getNowWeatherDate: function () {
        let date = new Date();
        return getFormateDate(date);
    },
    getWeatherDate: function (dateStr) {
        let date = new Date(dateStr);
        return getFormateDate(date);
    },
    getWeatherStyle: function (weather) {
        return weatherMaps.get(weather)
    }
}


module.exports = utils;

